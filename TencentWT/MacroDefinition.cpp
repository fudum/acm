#include "StdAfx.h"
#include "MacroDefinition.h"


MacroDefinition::MacroDefinition(void)
{
}


MacroDefinition::~MacroDefinition(void)
{
}

//请定义一个宏，比较两个数a、b的大小，不能使用大于、小于、if语句
int MacroDefinition::CompareAB(int a, int b)
{
	int max_num;
	int const shift1 = sizeof(int)*8-1;
	unsigned mark = (0x1<<shift1);
	if ((a-b)& mark)
	{
		max_num = b;
	}
	else
	{
		max_num = a;
	}
	return max_num;
}
