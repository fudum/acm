#include "StdAfx.h"
#include "SortAll.h"
#include <iostream>
#include <ctime>
using namespace std;


SortAll::SortAll(void)
{
}


SortAll::~SortAll(void)
{
}

//最大堆排序
void SortAll::MaxHeapSort()		
{
	//数组
	int data[10]={71,18,151,138,160 ,63 ,174, 169 ,79 ,78 };
	//构建一个大顶堆
	int heapsize = sizeof(data)/sizeof(data[0]);		//获取数组的长度
	for(int i=heapsize/2-1;i>=0;i--)
	{
		max_heapify(data,i,heapsize);
	}
	for (int i = heapsize-1;i>0;i--)
	{
		int t = data[0];
		data[0] = data[i];
		data[i] = t;
		max_heapify(data,0,i);
	}
}

//以某个节点为跟节点子树进行调整，调整为最大堆（递归操作）
void SortAll::max_heapify(int data[],int i, int heapsize)	
{
	int l = 2*i+1;
	int r = 2*i + 2;
	int larget = i;
	if (l< heapsize && data[l]>data[i])
	{
		larget = l;
	}
	if (r < heapsize && data[r] > data[i])
	{
		larget = r;
	}
	if (larget != i)
	{
		int temp = data[larget];
		data[larget] = data[i];
	}
	max_heapify(data, larget, heapsize);
}


int SortAll:: Partion(int a[], int p, int r)
{
	srand((unsigned)time(NULL));
	int e = rand()%(r-p+1)+ p;

	return e;
}


//快速排序
void SortAll::QuickSort(int a[], int p, int r)
{
	if (p < r)
	{
		int q = Partion(a, p ,q-1);
		QuickSort(a, p, q-1);
		QuickSort(a,q+1,r);
	}
}


//冒泡排序
void SortAll::BubbleSort(int arr[], int count)
{
	int i = count, j;
	int temp;
	while(i > 0)
	{
		for (j=0; j<i-1; j++)
		{
			if (arr[j] > arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] =temp;
			}
		}
		i--;
	}
}


//选择排序
void SortAll::SelectionSort(int arr[], int count)
{
	int k;
	int temp;
	for (int i = 0; i < count ; i++)
	{
		k = i;
		for (int j =i; j<count-1; j++)
		{
			if (arr[k] > arr[j+1])
			{
				k = j+1;
			}			
		}
		if (k != i)
		{
			temp = arr[k];
			arr[k] = arr[i];
			arr[i] = temp;
		}
	}
}
