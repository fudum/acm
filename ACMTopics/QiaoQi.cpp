#include "StdAfx.h"
#include "QiaoQi.h"
#include <iostream>
using namespace std;


QiaoQi::QiaoQi(void)
{
}


QiaoQi::~QiaoQi(void)
{
}

/*
 *	函数名称：qiaoqiFunction
 * 函数功能：输入一个数 输出小于它的所有的包含7的 或者是7的倍数的数
 * 输入参数： 无
 * 输出参数：无
 * 作者：fudum
 * 日期：2013/09/23 8:17
 */
void QiaoQi::qiaoqiFunction()
{
	//请求输入
	cout<<"请输入一个整数：";
	int a  = 0;
	cin>>a;
	int iFlag = 0;
	for (int i = 7; i < a; i++)
	{
		//判断能否对7整除
		if ( i % 7 == 0)
		{
			cout<<i<<endl;
			iFlag = 1;
		}
		//判断是否包含七
		if (iFlag != 1)
		{
			int iTemp = i;
			while (iTemp)
			{
				if (iTemp % 10 == 7)
				{
					cout<<i<<endl;
					break;
				}
				else
				{
					iTemp = iTemp / 10;
				}
			}			
		}
		else
		{
			iFlag =0;
		}
	}
}

void QiaoQi::qiaoqiFunction1()
{
	int n , i ;
	printf("请输入一个大于0 小于3000整数：");
	scanf("%d", &n);
	for (int i = 7; i <n; i +=7)
	{
		printf("%d\n", i);
		if ((i % 10) < 7)
		{
			if (i / 10 * 10 +7 < n )
			{
				printf("%d\n", i / 10* 10 +7);
			}
		}
	}
}
