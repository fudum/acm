#include "stdafx.h"
#include "TreesOutSchool.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;


TreesOutSchool::TreesOutSchool(void)
{
}


TreesOutSchool::~TreesOutSchool(void)
{
}

//输出剩余的树的数目
void TreesOutSchool:: PrintLeftTrees()
{
	 ifstream fin("d:/Temp/data.txt");		//read file
	 string s; 
	 int lenth;
	 int group;
	 int treenum;
	 vector<UndergroundPoint> data;	
	 vector<UndergroundPoint> resultdata;
	 try
	 {
		 fin >> lenth;
		 fin >> group;	 
		 while( fin >> s ) 
		 {    
			 UndergroundPoint * up = new UndergroundPoint();
			 up->start = atoi(s.c_str());
			 fin >> s;
			 up->end = atoi(s.c_str());
			 data.push_back(*up);
		 }
		 fin.close();
	 }
	 catch(string & s_value)
	 {
		 cout<<"文件数据有错误~"<<endl;
		 return;
	 }

	 //judge the count of threes
	 while (data.size() > 0)
	 {
		 UndergroundPoint ugp = data[0];	
		 bool bflag = false;
		 for (int i = 0; i< data.size()-1; i++)
		 {
			 if (data[i+1].start < ugp.start )
			 {
				 if (data[i+1].end < ugp.start)
				 {
					 //no connection
				 }
				 else if (data[i+1].end >= ugp.start && data[i+1].end <= ugp.end)
				 {
					 //
					 bflag = true;
					 ugp.start = data[i+1].start;
				 }
				 else
				 {
					 //
					 bflag = true;
					 ugp.start = data[i+1].start;
					 ugp.end = data[i+1].end;
				 }
			 }
			 else if (data[i+1].start >= ugp.start && data[i+1].start <= ugp.end)
			 {
				 if (data[i+1].end <= ugp.end)
				 {
					 //
					 bflag = true;
				 }
				 else
				 {
					 //
					 bflag = true;
					 ugp.end = data[i+1].end;
				 }
			 }
			 else
			 {
				 //no connection
			 }
			 if (bflag == true)
			 {
				//delete element of data	
				 data.erase(data.begin()+i+1);
				 bflag = false;
			 }
			 
		 }
		resultdata.push_back(ugp);
		data.erase(data.begin());
	 }
	 //calculate the count
	 treenum = lenth +1;
	 for (int i =0; i< resultdata.size(); i++)
	 {
		 treenum -= (resultdata[i].end - resultdata[i].start +1);
	 }

	 //write into file
	 ofstream file;
	 file.open("d:/Temp/output.txt");
	 file<<treenum;
	 file.close();	 
}
